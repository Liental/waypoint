# Change Log

All notable changes to the "waypoint" extension will be documented in this file.

## [1.0.2]
- Fixed character position tracking
- Added a remove function

## [1.0.1]
- Changed the quick pick file path so it's relative to the workspace
- Selecting a waypoint now scrolls to its position
- Added placeholders

## [1.0.0]
- Initial release
- Add a waypoint to the list
- Move to a waypoint from the list
- Track changes to adjust the waypoint position
