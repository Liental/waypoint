import { TextEditor, QuickPickItem, Position } from 'vscode';

export interface Waypoint extends QuickPickItem {
  position: Position;
  editor: TextEditor;
  lastSelected: Date;
  description: string;
  isUntitled: boolean;
  lineLength: number;
}
