import { Waypoint } from './waypoint.interface';
import { Position, window, workspace, commands, Selection, ExtensionContext, TextEditor, TextDocument } from 'vscode';

export function activate(context: ExtensionContext) {
	const waypoints: Waypoint[] = [];
	const editorLength: Map<string, Position> = new Map();

	const changeListener = workspace.onDidChangeTextDocument(editor => {
		const change = editor.contentChanges[0];
		const start = change.range.start;
		const text = editor.document.getText();
		const uriPath = editor.document.uri.path;

		waypoints
			.filter(waypoint => start.isBeforeOrEqual(waypoint.position))
			.forEach(waypoint => {
				const cachePosition = editorLength.get(uriPath);

				if (cachePosition) {
					const editorPosition = editor.document.positionAt(text.length);
					const lineDifference = editorPosition.line - cachePosition.line;
					const line = waypoint.position.line + lineDifference;

					// Compare the previous waypoint line length to the current one 
					// and add the character difference
					const lineLength = text.split('\n')[line].length;
					const character = waypoint.position.character + (lineLength - waypoint.lineLength);

					if (waypoint.isUntitled) {
						const labelReplace = `@${waypoint.position.line + 1}:${waypoint.position.character + 1}`;
						waypoint.label = waypoint.label.replace(labelReplace, `@${line + 1}:${character + 1}`);
					}

					const position = new Position(line, character);
					waypoint.position = position;
					waypoint.lineLength = lineLength;
				}
			});

		editorLength.set(uriPath, editor.document.positionAt(text.length));
	});

	const remove = commands.registerCommand('waypoint.remove', () => {
		const names = waypoints.sort((a, b) => a.lastSelected > b.lastSelected ? -1 : 1);

		window.showQuickPick(names, { matchOnDescription: true, placeHolder: 'Select a waypoint to remove' })
			.then(waypoint => {
				if (waypoint) {
					const index = waypoints.findIndex(wp => wp === waypoint);
					waypoints.splice(index, 1);
				}
			});
	});

	const add = commands.registerCommand('waypoint.add', () => {
		const editor = window.activeTextEditor;

		if (editor) {
			const line = editor.selection.active.line;
			const column = editor.selection.active.character;
			const name = editor.document.fileName.split('\\').pop();
			const label = `${name}@${line + 1}:${column + 1}`;
			const date = new Date();

			window.showInputBox({ value: label, placeHolder: 'New waypoint title' }).then(name => {
				// Get the current line length so we can compare it later
				// in order to check the character difference
				const lineLength = editor.document.getText().split('\n')[line].length;

				if (name && name.length > 0) {
					const waypoint: Waypoint = {
						editor,
						lineLength,
						lastSelected: date,
						isUntitled: name === label,
						label: name ? name : label,
						position: new Position(line, column),
						description: workspace.asRelativePath(editor.document.fileName)
					};

					waypoints.push(waypoint);
				}
			});

			const text = editor.document.getText();
			editorLength.set(editor.document.uri.path, editor.document.positionAt(text.length));
		}
	});

	const move = commands.registerCommand('waypoint.move', () => {
		const names = waypoints.sort((a, b) => a.lastSelected > b.lastSelected ? -1 : 1);

		window.showQuickPick(names, { matchOnDescription: true, placeHolder: 'Select a waypoint to move to' })
			.then(waypoint => {
				if (waypoint) {
					window.showTextDocument(waypoint.editor.document, 0)
						.then(() => {
							const selection = new Selection(waypoint.position, waypoint.position);
							const editor = window.activeTextEditor;

							if (editor) {
								editor.selection = selection;
								editor.revealRange(selection);
								waypoint.lastSelected = new Date();
							}
						});
				}
			});
	});

	context.subscriptions.push(changeListener);
	context.subscriptions.push(add);
	context.subscriptions.push(move);
	context.subscriptions.push(remove);
}

export function deactivate() { }
