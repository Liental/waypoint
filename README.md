# Waypoint

This extension is meant to make your coding experience easier by allowing you to create waypoints in your code. You can place it anywhere and easily come back to the place of interest!

## Features

Place a waypoint in your code on a line and character you often visit to easily come back to it whenever and from wherever you want!

The position of a waypoint will be adjusted while you're coding so you can always come back to the right place after the file has been changed.

## Extension Settings

This extension contributes the following settings:

* `waypoint.add`: add a new waypoint
* `waypoint.move`: focus the editor on the selected waypoint
* `waypoint.remove`: removes a waypoint

-----------------------------------------------------------------------------------------------------------
